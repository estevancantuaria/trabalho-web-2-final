<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        
        <c:forEach items="${errors}" var="erro">
        <p style=" color: red">${erro.message}</p><br>
    </c:forEach>
        <table class="table table-hover">
    <c:forEach items="${produtos}" var="p">
                    <tr>

                        <td>
                            <form action="${linkTo[VendasController].addItem}" method="post">
                                ${p.id}
                                ${p.descricao}
                                <input type="hidden" name="item.produto.id" value="${p.id}">
                                <input type="hidden" name="item.produto.descricao" value="${p.descricao}">
                                <input type="text" readonly="" name="item.produto.valor" value="${p.valor}">
                                <input type="text" name="item.qtd">
                                <input type="submit" value="Comprar">

                            </form>
                        </td>
                    </tr>
                </c:forEach>
        
                    <td><h2>Lista de compras</h2></td><br>
                    
                    <c:forEach items="${vendaprod.itens}" var="item" varStatus="i">
                        
                    <Tr>
                        
                        <td>  ${i.index+1} </td>
                        <td> ${item.produto.descricao}</td>
                        <td> ${item.produto.valor}</td>
                        <td> ${item.qtd}</td>
                        <td> ${item.total()}</td>
                    </Tr>  
                        
                </c:forEach>  
        </table>    
                    <br>
                    <a href="${linkTo[VendasController].salvarVenda}" class="btn btn-primary">CONCLUIR</a>
                    <a  href="${linkTo[ClientesController].logado}" class="btn btn-light">Voltar</a><br>
        
    </body>
</html>
