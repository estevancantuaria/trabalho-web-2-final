<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <style type="text/css">
        .ct{margin-top: 100px;}
    </style>
    <body>
        

        <c:forEach items="${errors}" var="erro">
            <p style=" color: red">${erro.message}</p><br>
        </c:forEach>

        <div class="container card ct"> 
            <h2>Formulario deProdutos</h2>
            <form action="${linkTo[ProdutosController].salvar}" method="post" >

                <input type="number" name ="produto.id" hidden="" value="${produto.id}">
                <label>Descrição: </label><br>
                <input type="text" name="produto.descricao" value="${produto.descricao}" class="form-group"><br>
                <label>Valor: </label><br>
                <input type="number" name="produto.valor" class="form-group" value="${produto.valor}"><br>
                <input type="submit" value="Enviar" class="btn btn-primary">

            </form>
        </div>
               
    </body>
</html>
