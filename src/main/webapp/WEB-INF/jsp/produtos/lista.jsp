<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <h1>Estoque de produtos</h1>
        
        <table class="table">
            <tr>
                <td>ID</td>
                <td>Descrição</td>
                <td>Valor</td>
                <td>Excluir</td>
                <td>Alterar</td>
            </tr>
    <c:forEach items="${produtoList}" var="produto">
        <tr>
            <td>${produto.id}</td> 
            <tD>${produto.descricao}</tD>
            <td>${produto.valor}</tD>
            <tD><a href="${linkTo[ProdutosController].excluir}?id=${produto.id}">Excluir</a></td>
            <tD><a href="${linkTo[ProdutosController].alterar}?id=${produto.id}">Alterar</a></td>
        </tr>
    </c:forEach>
        </table><br><br>
        <a  href="${linkTo[ClientesController].logado}" class="btn btn-primary" >Voltar</a>
         <a href="${linkTo[ProdutosController].form}" class="btn btn-primary">Inserir um novo produto</a>
        </div>
    </body>
</html>
