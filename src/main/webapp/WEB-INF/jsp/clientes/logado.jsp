<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <p class="badge-primary">Usuario logado: ${clienteSession.cliente.nome}</p><br>
        <h1>Painel de Controle</h1><br>
        <ul class="list-group">
            
            <li class="list-group-item"><a href="${linkTo[ProdutosController].form}">Formulario de Inserção de Produtos</a><br></li>

            <li class="list-group-item"><a href="${linkTo[ClientesController].lista}">Lista de Clientes</a><br></li> 
            <li class="list-group-item"><a href="${linkTo[ProdutosController].lista}">Estoque de Produtos</a><br></li>

            <li class="list-group-item"><a href="${linkTo[VendasController].formv}">Sistema de Vendas</a><br></li>
            
             <li class="list-group-item"><a href="${linkTo[VendasController].lista}">Lista de Vendas</a><br></li>
            
            <li class="list-group-item"><a href="${linkTo[ClientesController].logout}">Sair</a></li>
        </ul>
    </body>
</html>
