<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <style type="text/css">
        .ct{margin-top: 100px;}
    </style>
    <body>

        <div class="container card ct">
            <h1>Cadastro de Usuario</h1>
            <form action="${linkTo[ClientesController].salvar}" method="post">
                <input type="number" name ="cliente.id" hidden="" value="${cliente.id}" >
                <div class="form-group" >
                    <label for="">Usuario</label>
                    <input type="text" class="form-control"  name="cliente.nome" value="${cliente.nome}">
                </div>
                <div class="form-group">
                    <label>Senha</label>  
                    <input type="password" class="form-control" name="cliente.senha" value="${cliente.senha}"> 
                </div>
                <button type="submit" class="btn btn-primary">Cadastrar Login</button>
            </form>
        </div>


    </body>
</html>
