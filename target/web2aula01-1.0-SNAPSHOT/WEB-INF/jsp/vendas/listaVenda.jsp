<%-- 
    Document   : listaVenda
    Created on : Dec 7, 2019, 5:05:46 AM
    Author     : Estevan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
                    <td>${vendacliente.id}</td>

                    <td>${vendacliente.dataVenda}</td>

                    <td>${vendacliente.cliente.nome}</td>
                    
                    <td>${vendacliente.total()}</td>
        
        
    <c:forEach items="${vendacliente.itens}" var="item" varStatus="i">

        <Tr>

            <td>  ${i.index+1} </td>
            <td> ${item.produto.descricao}</td>
            <td> ${item.produto.valor}</td>
            <td> ${item.qtd}</td>
            <td> ${item.total()}</td>
        </Tr>  

    </c:forEach>  
</body>
</html>
