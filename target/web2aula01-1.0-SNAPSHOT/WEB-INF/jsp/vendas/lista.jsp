<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            
            <h1>Lista de Vendas</h1><br><br>
            <table class="table table-hover">

                <thead>
                <td>Id da venda</td>
                <td>Data da venda</td>
                <td>Cliente</td>
                <td>Total da Venda</td>
                </thead>

                

                <c:forEach items="${vendaList}" var="vendas"> 
<tbody>
                    <td>${vendas.id}</td>

                    <td>${vendas.dataVenda}</td>

                    <td>${vendas.cliente.nome}</td>
                    
                    <td>${vendas.total()}</td>
                    
                    <td><a href="${linkTo[VendasController].listaVenda}?id=${vendas.id}">consultar</a><td>
   </tbody> 
                </c:forEach>
               

            </table>
            <a  href="${linkTo[ClientesController].logado}" class="btn btn-primary" >Voltar</a>
             <a href="${linkTo[VendasController].formv}" class="btn btn-primary">Inserir uma nova venda</a>
        </div>
        
    </body>
</html>
