<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <style type="text/css">
        .ct{margin-top: 250px;}
    </style>
    <body>

        <div class="container card ct">

            <a  href="${linkTo[ClientesController].login}" class="btn btn-primary">Logar no Sistema</a><br>

            <a href="${linkTo[ClientesController].form}" class="btn btn-primary">Cadastrar-se no Sistema</a><br>

        </div>

    </body>
</html>
