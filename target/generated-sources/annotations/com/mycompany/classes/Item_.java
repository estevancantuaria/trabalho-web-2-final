package com.mycompany.classes;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ {

	public static volatile SingularAttribute<Item, Double> qtd;
	public static volatile SingularAttribute<Item, Produto> produto;
	public static volatile SingularAttribute<Item, Long> id;

}

