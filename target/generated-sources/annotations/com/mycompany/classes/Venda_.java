package com.mycompany.classes;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Venda.class)
public abstract class Venda_ {

	public static volatile SingularAttribute<Venda, Cliente> cliente;
	public static volatile ListAttribute<Venda, Item> itens;
	public static volatile SingularAttribute<Venda, String> dataVenda;
	public static volatile SingularAttribute<Venda, Long> id;

}

